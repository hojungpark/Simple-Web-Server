from socket import *
from datetime import *

serverName = 'localhost'
serverPort = 12000

def sendRequest(method, url, version, headers={}, body = ''):
    clientSocket = socket(AF_INET, SOCK_STREAM)
    clientSocket.connect((serverName, serverPort))

    requestLine = f"{method} {url} {version}\r\n"
    headerLine = ''.join([f"{key}: {value}\r\n" for key, value in headers.items()])
    fullRequest = requestLine + headerLine + '\r\n' + body

    clientSocket.send(fullRequest.encode())
    response = clientSocket.recv(1024).decode()
    clientSocket.close()

    return response

print("Testing 304 Not Modified (return 200):")
requestTime = "Thu, 23 Nov 2023 12:00:00 GMT\r\n"
response = sendRequest(method='GET', url='/test.html', version='HTTP/1.1', headers={'If-Modified-Since': requestTime})
print(response)

print("Testing 304 Not Modified (return 304):")
requestTime = datetime.now().strftime("%a, %d %b %Y %H:%M:%S GMT")
response = sendRequest(method='GET', url='/test.html', version='HTTP/1.1', headers={'If-Modified-Since': requestTime})
print(response)

print("Testing 400 Bad Request:")
response = sendRequest(method='', url='/test.html', version='HTTP/1.1')
print(response)

print("Testing 400 Bad Request:")
response = sendRequest(method='GEt', url='/test.html', version='HTTP/1.1')
print(response)

print("Testing 400 Bad Request:")
response = sendRequest(method='delete', url='/test.html', version='HTTP/1.1')
print(response)

print("Testing 403 Forbidded:")
response = sendRequest(method='PUT', url='/test.html', version='HTTP/1.1')
print(response)

print("Testing 403 Forbidded:")
response = sendRequest(method='DELETE', url='/test.html', version='HTTP/1.1')
print(response)

print("Testing 404 Not Found:")
response = sendRequest(method='GET', url='/nonexistentfile.html', version='HTTP/1.1')
print(response)

print("Testing 411 Length Required (return 200):")
response = sendRequest(method='POST', url='/test.html', version='HTTP/1.1', headers={'Content-Length': 300}, 
body="""
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="author" content="">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

</head>

<body>

  <p>Congratulations! Post Method works!</p>

</body>

</html>
""")
print(response)

print("Testing 411 Length Required (return 411):")
response = sendRequest(method='POST', url='/test.html', version='HTTP/1.1',
body="""
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="author" content="">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

</head>

<body>

  <p>Congratulations! Post Method works!</p>

</body>

</html>
""")
print(response)

