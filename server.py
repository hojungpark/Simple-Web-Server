from socket import *
from datetime import *
import os
import threading

# Constants
HTTP_200 = '200 OK'
HTTP_304 = '304 Not Modified'
HTTP_400 = '400 Bad Request'
HTTP_403 = '403 Forbidden'
HTTP_404 = '404 Not Found'
HTTP_411 = '411 Length Required'

# Initialize TCP Server Socket
serverSocket = socket(AF_INET, SOCK_STREAM)
serverPort = 12000
serverSocket.bind(('', serverPort))
serverSocket.listen(1)
print(f"Server is listening on port {serverPort}")

def constructResponse(status, body=''):
    response = f"HTTP/1.1 {status}\r\n"
    if body:
        response += "Content-Type: text/html\r\n"
        response += f"Content-Length: {len(body)}\r\n"
    response += "Connection: close\r\n\r\n"
    response += body
    return response

def handleRequest(connectionSocket):
    try:
        request = connectionSocket.recv(1024).decode()
        requestLines = request.split('\r\n')
        requestLine = [element for element in requestLines[0].split(' ') if element != '']
        
        if len(requestLine) != 3:
            response = constructResponse(HTTP_400)
        else:
            method, url, _ = requestLine
            methods = ["GET", "POST", "HEAD", "PUT", "DELETE"]
            if method not in methods:
                response = constructResponse(HTTP_400)
            elif method in ["PUT", "HEAD", "DELETE"]:
                response = constructResponse(HTTP_403)
            else:
                filePath = '.' + url
                if not os.path.isfile(filePath):
                    response = constructResponse(HTTP_404)
                elif method == "POST" and "Content-Length" not in request:
                    response = constructResponse(HTTP_411)
                else:
                    if 'If-Modified-Since:' in request:
                        fileModTime = os.path.getmtime(filePath)
                        fileModTime = datetime.fromtimestamp(fileModTime)
                        clientRequestTime = datetime.strptime(requestLines[1][19:], "%a, %d %b %Y %H:%M:%S GMT")
                        if fileModTime < clientRequestTime:
                            response = constructResponse(HTTP_304)
                        else:
                            with open(filePath, 'r') as url:
                                response = constructResponse(HTTP_200, url.read())
                    elif method == "POST":
                        requestBody = request.split('\r\n\r\n')[1]
                        with open('test.html', 'w') as url:
                            url.write(requestBody)
                        with open(filePath, 'r') as url:
                            response = constructResponse(HTTP_200, url.read())
                    else:
                        with open(filePath, 'r') as url:
                            response = constructResponse(HTTP_200, url.read())

        connectionSocket.send(response.encode())
    except Exception as e:
        print(f"Error: {e}")
    finally:
        connectionSocket.close()



while True:
    connectionSocket, addr = serverSocket.accept()
    clientThread = threading.Thread(target=handleRequest, args=(connectionSocket,))
    clientThread = threading.Thread(target=handleRequest, args=(connectionSocket,))
    clientThread.start()
    